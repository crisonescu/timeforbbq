package com.cristian.timeforbbq.View;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cristian.timeforbbq.Model.Forecast;
import com.cristian.timeforbbq.Model.Query;

import java.util.List;

/**
 * Created by Cristian on 02.08.2017.
 */
public class WeatherLayoutUpdater {

    private Context context;

    String conditionCode;
    String conditionTemperature;
    String conditionDate;
    String conditionDescription;
    String location;

    List<Forecast> forecastList;

    public WeatherLayoutUpdater(Context context, Query weatherResults){

        this.context = context;
        this.conditionCode = weatherResults.getResults().getChannel().getItem().getCondition().getCode();
        this.conditionTemperature = weatherResults.getResults().getChannel().getItem().getCondition().getTemp();
        this.conditionDate = weatherResults.getResults().getChannel().getItem().getCondition().getDate();
        this.conditionDescription = weatherResults.getResults().getChannel().getItem().getCondition().getCondText();
        this.location = weatherResults.getResults().getChannel().getLocation().getCity();

        this.forecastList = weatherResults.getResults().getChannel().getItem().getForecast();

    }

    public void updateLayout(LinearLayout fiveDaysForecast, TextView weatherIcon, TextView temperature, TextView date, TextView conditionToday, TextView location){

        weatherIcon.setText(context.getResources().getText(context.getResources().getIdentifier("code_" + conditionCode, "string", context.getPackageName())));
        temperature.setText(conditionTemperature + " " + '\u00B0');
        temperature.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Bold.ttf"));
        date.setText(conditionDate);
        conditionToday.setText(conditionDescription);
        conditionToday.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Light.ttf"));
        location.setText(this.location);
        location.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Regular.ttf"));


        for(int index = 0; index < 5; index++){
            //ignore first day because we already have the data.
            if(!forecastList.get(index).getDay().equals(forecastList.get(0).getDay())) {
                LinearLayout day = new LinearLayout(context.getApplicationContext());
                LinearLayout temperatures = new LinearLayout(context.getApplicationContext());

                TextView codeTextView = new TextView(context.getApplicationContext());
                codeTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/weather.ttf"));
                codeTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
                codeTextView.setGravity(Gravity.CENTER);
                codeTextView.setText(context.getResources().getText(context.getResources().getIdentifier("code_" + String.valueOf(forecastList.get(index).getCode()), "string", context.getPackageName())));

                TextView dayTextView = new TextView(context.getApplicationContext());
                dayTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Bold.ttf"));
                dayTextView.setGravity(Gravity.CENTER);
                dayTextView.setText(forecastList.get(index).getDay());

                TextView highTextView = new TextView(context.getApplicationContext());
                highTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Regular.ttf"));
                highTextView.setText(String.valueOf(forecastList.get(index).getHigh()) + " " + '\u00B0');

                TextView lowTextView = new TextView(context.getApplicationContext());
                lowTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Regular.ttf"));
                lowTextView.setText(String.valueOf(forecastList.get(index).getLow()) + " " + '\u00B0');

                TextView descriptionTextView = new TextView(context.getApplicationContext());
                descriptionTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Light.ttf"));
                descriptionTextView.setGravity(Gravity.CENTER);
                descriptionTextView.setText(forecastList.get(index).getFcText());
                descriptionTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

                temperatures.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
                temperatures.setOrientation(LinearLayout.VERTICAL);
                temperatures.setGravity(Gravity.CENTER);
                temperatures.addView(highTextView);
                temperatures.addView(lowTextView);

                day.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER));
                day.setOrientation(LinearLayout.VERTICAL);
                day.setPadding(10,0,10,0);
                day.setGravity(Gravity.CENTER);
                day.addView(dayTextView);
                day.addView(codeTextView);
                day.addView(temperatures);
                day.addView(descriptionTextView);

                fiveDaysForecast.addView(day);
            }
        }

    }

}
