package com.cristian.timeforbbq.View;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cristian.timeforbbq.Model.Forecast;
import com.cristian.timeforbbq.R;
import com.cristian.timeforbbq.WeatherActivity;

import java.util.List;

/**
 * Created by Cristian on 29.07.2017.
 */
public class CustomDialog {

    private LinearLayout fiveDaysForecast;
    private Activity applicationContext;

    public CustomDialog(WeatherActivity weatherActivity) {
        this.applicationContext = weatherActivity;
    }

    private void displayData(List<Forecast> forecastList){
        fiveDaysForecast.removeAllViews();

        for(int index = 0; index < 5; index++) {
            LinearLayout day = new LinearLayout(applicationContext);
            day.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setType("vnd.android.cursor.item/event");
                    intent.putExtra("title", "Chill and BBQ");
                    intent.putExtra("description", "BBQ set on this day");
                    //intent.putExtra("beginTime", forecastDay.getDate().getTime());
                    intent.putExtra("allDay", true);
                    applicationContext.startActivity(intent);

                    //new NotificationBuilder(activity).notificationShow("Chill and BBQ", forecastDay.getDate());
                }
            });

            TextView codeTextView = new TextView(applicationContext);
            codeTextView.setGravity(Gravity.CENTER);
            codeTextView.setTypeface(Typeface.createFromAsset(applicationContext.getAssets(), "font/weather.ttf"));
            codeTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
            codeTextView.setText(applicationContext.getResources().getText(applicationContext.getResources().getIdentifier("code_" + forecastList.get(index).getCode(), "string", applicationContext.getPackageName())));

            TextView dayTextView = new TextView(applicationContext);
            dayTextView.setGravity(Gravity.CENTER);
            dayTextView.setTypeface(Typeface.createFromAsset(applicationContext.getAssets(), "font/Comfortaa-Bold.ttf"));
            dayTextView.setText(forecastList.get(index).getDay());

            TextView highTextView = new TextView(applicationContext);
            highTextView.setGravity(Gravity.CENTER);
            highTextView.setTypeface(Typeface.createFromAsset(applicationContext.getAssets(), "font/Comfortaa-Regular.ttf"));
            highTextView.setText(String.valueOf(forecastList.get(index).getHigh()) + " " + '\u00B0');

            TextView lowTextView = new TextView(applicationContext);
            lowTextView.setGravity(Gravity.CENTER);
            lowTextView.setTypeface(Typeface.createFromAsset(applicationContext.getAssets(), "font/Comfortaa-Regular.ttf"));
            lowTextView.setText(String.valueOf(forecastList.get(index).getLow()) + " " + '\u00B0');

            TextView descriptionTextView = new TextView(applicationContext);
            descriptionTextView.setGravity(Gravity.CENTER);
            descriptionTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
            descriptionTextView.setTypeface(Typeface.createFromAsset(applicationContext.getAssets(), "font/Comfortaa-Light.ttf"));
            descriptionTextView.setText(forecastList.get(index).getFcText());

            day.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL));
            day.setOrientation(LinearLayout.VERTICAL);
            day.addView(codeTextView);
            day.addView(dayTextView);
            day.addView(highTextView);
            day.addView(lowTextView);
            day.addView(descriptionTextView);
            day.setWeightSum(0.1f);


            fiveDaysForecast.addView(day);
            fiveDaysForecast.setGravity(Gravity.CENTER_VERTICAL);
        }
    }


    public void showDialog(String msg, List<Forecast> forecastList){

        final Dialog dialog = new Dialog(applicationContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_layout);
        fiveDaysForecast = dialog.findViewById(R.id.result_layout);
        displayData(forecastList);

        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
}
