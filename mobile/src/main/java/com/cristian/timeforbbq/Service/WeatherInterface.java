package com.cristian.timeforbbq.Service;

import com.cristian.timeforbbq.Model.WeatherResults;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Cristian on 04.08.2017.
 */

public interface WeatherInterface {

    /* da, stiu.. nici mie nu imi place.. dar e o problema de encodare
     a query-ului... de la retrofit

     EXPECTATIONS:
     "select * from weather.forecast " +
        "where woeid in " +
            "(select woeid from geo.places(1) " +
                    "where text=\"Timisoara, RO\") " +
        "and u = 'c'" +
        "&format=json";
     VS REALITY:
     */
    
    @GET("/v1/public/yql/?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text=\"Timisoara,%20RO\")%20and%20u%20=%20%27c%27&format=json")
    Call<WeatherResults> getWeatherResults();

}
