package com.cristian.timeforbbq.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Cristian on 04.08.2017.
 */

public class Atmosphere {
    @SerializedName("humidity")
    @Expose
    private String humidity;

    @SerializedName("pressure")
    @Expose
    private String pressure;
}
