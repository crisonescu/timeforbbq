package com.cristian.timeforbbq.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Cristian on 04.08.2017.
 */

public class ChItem {
    @SerializedName("condition")
    @Expose
    private ItCondition condition;

    @SerializedName("forecast")
    @Expose
    private ArrayList<Forecast> forecast = new ArrayList<>();

    public ItCondition getCondition() {
        return condition;
    }

    public void setCondition(ItCondition condition) {
        this.condition = condition;
    }

    public ArrayList<Forecast> getForecast() {
        return forecast;
    }

    public void setForecast(ArrayList<Forecast> forecast) {
        this.forecast = forecast;
    }
}
