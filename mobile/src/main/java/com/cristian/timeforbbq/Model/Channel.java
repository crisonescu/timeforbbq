package com.cristian.timeforbbq.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Cristian on 04.08.2017.
 */

public class Channel{
    @SerializedName("location")
    @Expose
    private ChLocation location;

    @SerializedName("units")
    @Expose
    private Units units;

    @SerializedName("wind")
    @Expose
    private Wind wind;

    @SerializedName("atmosphere")
    @Expose
    private Atmosphere atmosphere;

    @SerializedName("item")
    @Expose
    private ChItem item;

    public ChLocation getLocation() {
        return location;
    }

    public void setLocation(ChLocation location) {
        this.location = location;
    }

    public Units getUnits() {
        return units;
    }

    public void setUnits(Units units) {
        this.units = units;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Atmosphere getAtmosphere() {
        return atmosphere;
    }

    public void setAtmosphere(Atmosphere atmosphere) {
        this.atmosphere = atmosphere;
    }

    public ChItem getItem() {
        return item;
    }

    public void setItem(ChItem item) {
        this.item = item;
    }
}
