package com.cristian.timeforbbq.Logic;

/**
 * Created by Cristian on 29.07.2017.
 */
public enum SuitableByConditionCodeEnum {
    NO_NETWORK, /* if one has this value, everything has to stop */
    NOT_OK,     /* priority 0 */
    TAKE_CARE,  /* priority 50 */
    BBQ_TIME    /* priority 100 */
}
