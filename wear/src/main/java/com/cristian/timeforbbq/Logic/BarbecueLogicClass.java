package com.cristian.timeforbbq.Logic;

import com.cristian.timeforbbq.Model.Forecast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cristian on 28.07.2017.
 */
public class BarbecueLogicClass {
    List<Forecast> forecastList;

    public BarbecueLogicClass(List<Forecast> forecastList) {
        this.forecastList = forecastList;
    }



    /**
     * This will return the priority of the days from day 0 to day 4 (5 days)
     * The higher the priority number is. The higher chances are to have a great
     * day at BBQ.
     */
    public String mathForGreatestDayForBBQ() {
        List<Integer> priorityList = new ArrayList<>();
        if(0 < forecastList.size()) {
            for (int i = 0; i < 5; i++) {
                Forecast index = forecastList.get(i);
                int priority = 0;

                if ((isWeatherSuitableByConditionCode(Integer.parseInt(index.getCode())).equals(SuitableByConditionCodeEnum.BBQ_TIME))) {
                    priority = 100;
                } else if (isWeatherSuitableByConditionCode(Integer.parseInt(index.getCode())).equals(SuitableByConditionCodeEnum.TAKE_CARE)) {
                    priority = 50;
                } else if (isWeatherSuitableByConditionCode(Integer.parseInt(index.getCode())).equals(SuitableByConditionCodeEnum.NOT_OK)) {
                    priority = 0;
                } else {
                    //something failed on the user side. stop the process.
                    break;
                }

            /* Get the average of Min and High to see if there are expected great differences in temperature */
                priority += isWeatherSuitableByAverageLowestHighest(Integer.parseInt(index.getHigh()), Integer.parseInt(index.getLow()));
                priorityList.add(priority);
            }
        }
        return prepareResult(priorityList);
    }

    private String prepareResult(List<Integer> priorityList){
        List<Integer> highValues = new ArrayList<>(); /* over 100 priority */
        List<Integer> mediumValues = new ArrayList<>(); /* over 50 priority */
        List<Integer> lowestValues = new ArrayList<>(); /* under 50 priority */

        String message = "Looks like ";

        for(int i = 0; i < priorityList.size(); i++)
        {
            if(100 <= priorityList.get(i)){
                highValues.add(i);
            }else if(50 <= priorityList.get(i) &&
                     100 > priorityList.get(i)){
                mediumValues.add(i);
            }else {
                lowestValues.add(i);
            }
        }

         /* HIGH - EMPTY
         *  MEDIUM - EMPTY
         *  LOWEST - HAS VALUES*/
        if((highValues.isEmpty())     &&
           (mediumValues.isEmpty())   &&
           !(lowestValues.isEmpty())){
            message = "there are only bad news about the BBQ :(";

         /* HIGH - HAS VALUES
         *  MEDIUM - HAS VALUES
         *  LOWEST - EMPTY*/
        }else if(!(highValues.isEmpty())    &&
                 !(mediumValues.isEmpty())  &&
                 (lowestValues.isEmpty())){
            for (int hindex = 0; hindex < highValues.size(); hindex++){
                message = message + forecastList.get(highValues.get(hindex)).getDay() + ", ";
            }
            message = message + "are the best days for a BBQ. Beside them also ";
            for (int mindex = 0; mindex < mediumValues.size(); mindex++){
                message = message + forecastList.get(mediumValues.get(mindex)).getDay() + ", ";
            }
            message = message + "looks ok.";

         /* HIGH - HAS VALUES
         *  MEDIUM - EMPTY
         *  LOWEST - EMPTY*/
        }else if(!(highValues.isEmpty())    &&
                (mediumValues.isEmpty())    &&
                (lowestValues.isEmpty())){
            for (int hindex = 0; hindex < highValues.size(); hindex++){
                message = message + forecastList.get(highValues.get(hindex)).getDay() + ", ";
            }
            message = message + "are the best days for a BBQ.";

        /*  HIGH - EMPTY
         *  MEDIUM - HAS VALUES
         *  LOWEST - EMPTY*/
        }else if((highValues.isEmpty())      &&
                !(mediumValues.isEmpty())   &&
                (lowestValues.isEmpty())){
            for (int mindex = 0; mindex < mediumValues.size(); mindex++){
                message = message + forecastList.get(mediumValues.get(mindex)).getDay() + ", ";
            }
            message = message + "looks ok. But you should be careful.";


        /*  HIGH - EMPTY
         *  MEDIUM - EMPTY
         *  LOWEST - EMPTY*/
        }else if((highValues.isEmpty())      &&
                (mediumValues.isEmpty())   &&
                (lowestValues.isEmpty())){
            message = message + "looks like there is an error.";

        /*  HIGH - HAS VALUES
         *  MEDIUM - HAS VALUES
         *  LOWEST - HAS VALUES*/
        }else if(!(highValues.isEmpty())      &&
                !(mediumValues.isEmpty())   &&
                !(lowestValues.isEmpty())) {
            for (int hindex = 0; hindex < highValues.size(); hindex++){
                message = message + forecastList.get(highValues.get(hindex)).getDay() + ", ";
            }
            message = message + "are the best days for a BBQ. Beside them also ";
            for (int mindex = 0; mindex < mediumValues.size(); mindex++){
                message = message + forecastList.get(mediumValues.get(mindex)).getDay() + ", ";
            }
            message = message + "looks ok. But expect bad days ";
            for (int lindex = 0; lindex < lowestValues.size(); lindex++){
                message = message + forecastList.get(lowestValues.get(lindex)).getDay() + ", ";
            }
            message = message + ".";

        /*  HIGH - EMPTY
         *  MEDIUM - HAS VALUES
         *  LOWEST - HAS VALUES*/
        }else if((highValues.isEmpty())      &&
                !(mediumValues.isEmpty())   &&
                !(lowestValues.isEmpty())) {
            for (int mindex = 0; mindex < mediumValues.size(); mindex++){
                message = message + forecastList.get(mediumValues.get(mindex)).getDay() + ", ";
            }
            message = message + "looks ok. But expect bad days ";
            for (int lindex = 0; lindex < lowestValues.size(); lindex++){
                message = message + forecastList.get(lowestValues.get(lindex)).getDay() + ", ";
            }
            message = message + ".";

        /*  HIGH - HAS VALUES
         *  MEDIUM - EMPTY
         *  LOWEST - HAS VALUES*/
        }else if(!(highValues.isEmpty())     &&
                (mediumValues.isEmpty())    &&
                !(lowestValues.isEmpty())) {
            message = message + "weather is great for ";
            for (int hindex = 0; hindex < highValues.size(); hindex++){
                message = message + forecastList.get(highValues.get(hindex)).getDay() + ", ";
            }
            message = message + "but expect bad days on ";
            for (int lindex = 0; lindex < lowestValues.size(); lindex++){
                message = message + forecastList.get(lowestValues.get(lindex)).getDay() + ", ";
            }
            message = message + ".";
        }

        return message;
    }

    private SuitableByConditionCodeEnum isWeatherSuitableByConditionCode(int conditionCode) {
        if((36 == conditionCode) ||
           (31 == conditionCode) ||
           (32 == conditionCode) ||
           (33 == conditionCode) ||
           (34 == conditionCode)){
            return SuitableByConditionCodeEnum.BBQ_TIME;
        }else if((29 == conditionCode) ||
                (30 == conditionCode) ||
                (19 == conditionCode)){
            return SuitableByConditionCodeEnum.TAKE_CARE;
        }else if(3200 == conditionCode){
            return SuitableByConditionCodeEnum.NO_NETWORK;
        }
        else{
            return SuitableByConditionCodeEnum.NOT_OK;
        }
    }

    private int isWeatherSuitableByAverageLowestHighest(int high, int low)
    {
        return ( (high+low)/2 );
    }
}
