package com.cristian.timeforbbq.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Cristian on 04.08.2017.
 */

public class Forecast {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("day")
    @Expose
    private String day;

    @SerializedName("high")
    @Expose
    private String high;

    @SerializedName("low")
    @Expose
    private String low;

    @SerializedName("text")
    @Expose
    private String fcText;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getFcText() {
        return fcText;
    }

    public void setFcText(String fcText) {
        this.fcText = fcText;
    }
}
