package com.cristian.timeforbbq;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cristian.timeforbbq.Logic.BarbecueLogicClass;
import com.cristian.timeforbbq.Model.Forecast;
import com.cristian.timeforbbq.Model.Query;
import com.cristian.timeforbbq.Model.WeatherResults;
import com.cristian.timeforbbq.Service.WeatherInterface;
import com.cristian.timeforbbq.View.CustomDialog;
import com.cristian.timeforbbq.View.WeatherLayoutUpdater;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherWearActivity extends Activity {

    private LinearLayout fiveDaysForecast;
    private LinearLayout todayWeather;

    private TextView weatherIcon;
    private TextView temperature;
    private TextView conditionToday;
    private TextView location;
    private TextView date;
    private Button shouldIBBQ;

    private CustomDialog customDialog;
    private ProgressDialog dialog;
    private List<Forecast> forecastList;

    private static final String BASE_URL = "https://query.yahooapis.com";
    private static Retrofit retrofit = null;

    public Query weatherResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_wear);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                forecastList    = new ArrayList<>();

                fiveDaysForecast = findViewById(R.id.five_days_forecast);
                todayWeather     = findViewById(R.id.today_weather);

                weatherIcon     = findViewById(R.id.weatherIcon);
                temperature     = findViewById(R.id.temperature);
                conditionToday  = findViewById(R.id.conditionToday);
                location        = findViewById(R.id.location);
                date            = findViewById(R.id.date);

                Typeface type = Typeface.createFromAsset(getAssets(),"font/weather.ttf");
                weatherIcon.setTypeface(type);
                weatherIcon.setText(getApplicationContext().getString(R.string.code_3200));
            }
        });
        dialog = new ProgressDialog(this);

        refreshData("INIT");
    }

    public void shouldIBBQ(View v){
        clearAll();

        refreshData("BBQ_TIME");
        customDialog = new CustomDialog(this);

    }

    public void refreshData(final String state){
        weatherResults = null;
        dialog.setMessage(getResources().getText(getResources().getIdentifier("loading_weather_data", "string", getPackageName())));
        dialog.show();

        new AsyncTask<Object, Object, Void>() {
            @Override
            protected Void doInBackground(Object... strings) {
                WeatherInterface weatherService = getClient().create(WeatherInterface.class);

                Call<WeatherResults> call = null;

                call = weatherService.getWeatherResults();
                call.enqueue(new Callback<WeatherResults>() {
                    @Override
                    public void onResponse(Call<WeatherResults>call, Response<WeatherResults> response) {
                        if(200 == response.code()) {  //SUCCESS status code is 200
                            weatherResults = response.body().getQuery();
                            forecastList = weatherResults.getResults().getChannel().getItem().getForecast();

                            new WeatherLayoutUpdater(getApplicationContext(), weatherResults).updateLayout(fiveDaysForecast, weatherIcon, temperature, date, conditionToday, location);
                            dialog.hide();

                            if((forecastList != null) &&
                                    ("BBQ_TIME".equals(state))){
                                customDialog.showDialog(new BarbecueLogicClass(forecastList).mathForGreatestDayForBBQ(), forecastList);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<WeatherResults> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("FAILED AT DATA RECEIVE", t.toString());
                        dialog.hide();
                        Toast.makeText(getApplicationContext(), "FAILE", Toast.LENGTH_LONG).show();
                    }
                });
                return null;
            }
        }.execute();
    }

    private static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public void clearAll(){
        forecastList    = new ArrayList<>();
        fiveDaysForecast.removeAllViews();
        weatherIcon.setText(getResources().getText(getResources().getIdentifier("code_3200", "string", getPackageName())));
        temperature.setText("N/A" + " " + '\u00B0' + "N/A");
        date.setText(getResources().getText(getResources().getIdentifier("NA", "string", getPackageName())));
        conditionToday.setText(getResources().getText(getResources().getIdentifier("NA", "string", getPackageName())));
        location.setText(getResources().getText(getResources().getIdentifier("NA", "string", getPackageName())));

    }
}
